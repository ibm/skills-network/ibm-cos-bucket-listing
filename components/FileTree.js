import React from 'react';
import { Tree, Popover, message, Typography } from 'antd';
import { config } from '../config';

function FileItem({ filename, url }) {
  return (
    <Typography.Text
      copyable={{
        text: encodeURI(url),
        onCopy: () => message.success('Copied to clipboard'),
      }}
    >
      {filename}
    </Typography.Text>
  );
}

function buildQuickLabsLaunchUrl(baseUrl, fileUrl) {
  const url = new URL(baseUrl);
  url.search = new URLSearchParams({ md_instructions_url: fileUrl }).toString(); // eslint-disable-line camelcase
  return url;
}

function HtmlFileItem({ filename, url }) {
  return (
    <Popover
      content={
        <div>
          <a
            onClick={async () => {
              await navigator.clipboard.writeText(
                `<p>In case you are having issues viewing the lab instructions below or prefer to view the instructions in a new browser tab, <a href="${url}" target="_blank">click here</a>.</p>\n<p><iframe src="${url}" width="100%" height="1200">In case you have trouble viewing the lab instructions above, you can also view them by <a href="${url}" target="_blank">clicking here</a></iframe></p>`
              );
              message.success('iframe code is copied to clipboard');
            }}
          >
            Copy
          </a>{' '}
          iframe code
        </div>
      }
    >
      <Typography.Text
        copyable={{
          text: encodeURI(url),
          onCopy: () => message.success('Copied to clipboard'),
        }}
      >
        {filename}
      </Typography.Text>
    </Popover>
  );
}

function QuickLabsFileItem({
  filename,
  cloudIdeUrl,
  dockerUrl,
  openShiftUrl,
  url,
}) {
  return (
    <Popover
      content={
        <div>
          Open in{' '}
          <a
            href={buildQuickLabsLaunchUrl(cloudIdeUrl, url)}
            target="_blank"
            rel="noopener noreferrer"
          >
            Cloud IDE
          </a>
          ,{' '}
          <a
            href={buildQuickLabsLaunchUrl(dockerUrl, url)}
            target="_blank"
            rel="noopener noreferrer"
          >
            Cloud IDE (with Docker)
          </a>{' '}
          or in{' '}
          <a
            href={buildQuickLabsLaunchUrl(openShiftUrl, url)}
            target="_blank"
            rel="noopener noreferrer"
          >
            Cloud IDE (with OpenShift)
          </a>
        </div>
      }
    >
      <Typography.Text
        copyable={{
          text: encodeURI(url),
          onCopy: () => message.success('Copied to clipboard'),
        }}
      >
        {filename}
      </Typography.Text>
    </Popover>
  );
}

function processTreeData(data) {
  return data.map((item) => {
    if (item.children?.length > 0) {
      return {
        key: item.key,
        title: item.title,
        children: processTreeData(item.children),
      };
    }

    const fileExtension = item.key.split('.').pop();
    return {
      key: item.key,
      selectable: false,
      title:
        config.sn.dockerLaunchUrl &&
        config.sn.openShiftLaunchUrl &&
        fileExtension === 'md' ? (
          <QuickLabsFileItem
            filename={item.title}
            url={item.publicUrl}
            cloudIdeUrl={config.sn.cloudIdeLaunchUrl}
            dockerUrl={config.sn.dockerLaunchUrl}
            openShiftUrl={config.sn.openShiftLaunchUrl}
          />
        ) : fileExtension === 'html' ? (
          <HtmlFileItem filename={item.title} url={item.publicUrl} />
        ) : (
          <FileItem filename={item.title} url={item.publicUrl} />
        ),
    };
  });
}

function FileTree({ treeData }) {
  const [showTree, setShowTree] = React.useState(false);
  React.useEffect(() => {
    setShowTree(true);
  }, [])

  const transformedTreeData = React.useMemo(() => processTreeData(treeData), [
    treeData,
  ]);

  if (!showTree) return null;

  return <Tree showLine treeData={transformedTreeData} />;
}

export default FileTree;
