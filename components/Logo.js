import React from 'react';
import { Typography } from 'antd';
import logo from '../logo.png';
import styles from './Logo.module.css';

const { Title } = Typography;

function Logo({ title }) {
  return (
    <>
      <a className={styles.logo}>
        <img src={logo} />
      </a>
      <Title className={styles.title} level={2}>
        {title}
      </Title>
    </>
  );
}

export default Logo;
