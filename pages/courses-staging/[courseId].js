import React from 'react';
import { useRouter } from 'next/router';
import { Empty, Spin, PageHeader as AntdPageHeader } from 'antd';
import { parse } from 'fast-xml-parser';
import { config } from '../../config';
import { buildFileTree } from '../../utils';
import FileTree from '../../components/FileTree';

async function get(path, queryParameters, options) {
  const url = new URL(path);
  url.search = new URLSearchParams(queryParameters).toString();
  return fetch(url.href, options);
}

function CourseAssetPage({ fileTreeData }) {
  const router = useRouter();

  if (router.isFallback) {
    return (
      <div
        style={{
          background: '#fff',
          padding: 24,
          minHeight: '60vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Spin size="large" />
      </div>
    );
  }

  return (
    <div
      style={{
        background: '#fff',
        padding: 24,
        minHeight: '60vh',
        ...(fileTreeData
          ? {}
          : {
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }),
      }}
    >
      {fileTreeData ? (
        <FileTree treeData={fileTreeData} />
      ) : (
        <Empty description="We are working hard to update your Asset Library, please check again in a minute." />
      )}
    </div>
  );
}

const PageHeader = () => {
  const router = useRouter();

  return (
    <AntdPageHeader
      subTitle={
        <span>
          Browse the Skills Network Asset Library for the project {router.query.courseId}. You can copy links to these assets to use these files in your course.
        </span>
      }
      ghost={false}
      style={{
        border: '1px solid rgb(235, 237, 240)',
      }}
    />
  );
};

export async function getStaticPaths() {
  return { paths: [], fallback: true };
}

/* eslint-disable camelcase */
export async function getStaticProps({ params }) {
  const { courseId } = params;

  try {
    const queryParameters = { delimiter: '/:1', prefix: courseId };
    const resp = await get(config.cos.stagingBucketUrl, queryParameters);
    const text = await resp.text();
    const json = parse(text);
    const objects = json?.ListBucketResult?.Contents || null;

    if (!objects) return { props: {}, revalidate: 1 };

    const files = Array.isArray(objects)
      ? objects.map((each) => each.Key)
      : [objects.Key];

    const fileTreeData = buildFileTree(
      files,
      `${config.cos.stagingBucketUrl}/${courseId}`
    );
    return { props: { fileTreeData }, revalidate: 1 };
  } catch (error) {
    console.error(error);
    return { props: {} };
  }
}
/* eslint-enable camelcase */

CourseAssetPage.PageHeader = PageHeader;

export default CourseAssetPage;
