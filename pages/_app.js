import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { Layout } from 'antd';
import Logo from '../components/Logo';
import { config } from '../config';
import './app.css';

const { Header, Content, Footer } = Layout;

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  const router = useRouter();

  if (Component.displayName === 'ErrorPage') {
    return <Component {...pageProps} />;
  }

  return (
    <Layout className="layout" style={{ minHeight: '100vh' }}>
      <Head>
        <title>{config.site.title}</title>
      </Head>
      <Header
        style={{
          background: '#fff',
          height: 'auto',
          boxShadow: '0 1px 4px rgba(0,21,41,.08)',
        }}
      >
        <Logo title={router.query.courseId || router.query.quicklabId} />
      </Header>
      {Component.PageHeader ? <Component.PageHeader /> : null}
      <Content style={{ padding: '0 50px', paddingTop: '48px' }}>
        <Component {...pageProps} />
      </Content>
      <Footer style={{ textAlign: 'center' }}>
        IBM Developer Skills Network © {new Date().getFullYear()}
      </Footer>
    </Layout>
  );
}
