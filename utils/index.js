import _each from 'lodash.foreach';
import _find from 'lodash.find';

function buildFileTree(paths, baseUrl) {
  const tree = [];

  _each(paths, (path) => {
    const pathParts = path.split('/');
    pathParts.shift(); // Remove first blank element from the parts array.

    let currentLevel = tree; // Initialize currentLevel to root
    let currentPath = null;
    _each(pathParts, (part) => {
      // Check to see if the path already exists.
      const existingPath = _find(currentLevel, {
        title: part,
      });

      currentPath = currentPath ? `${currentPath}/${part}` : part;

      if (existingPath) {
        currentLevel = existingPath.children;
      } else {
        const newPart = {
          title: part,
          key: currentPath,
          children: [],
          publicUrl: `${baseUrl}/${currentPath}`,
        };
        currentLevel.push(newPart);
        currentLevel = newPart.children;
      }
    });
  });

  return tree;
}

export { buildFileTree };
