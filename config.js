const config = {
  site: {
    title: 'Skills Network Asset Library',
    logoUrl: process.env.SITE_LOGO_URL,
  },
  cos: {
    bucketUrl: process.env.COS_BUCKET_URL,
    stagingBucketUrl: process.env.STAGING_COS_BUCKET_URL,
  },
  sn: {
    cloudIdeLaunchUrl: 'https://labs.cognitiveclass.ai/tools/theia',
    dockerLaunchUrl: process.env.NEXT_PUBLIC_SN_DOCKER_LAUNCH_URL,
    openShiftLaunchUrl: process.env.NEXT_PUBLIC_SN_OPENSHIFT_LAUNCH_URL,
  },
};

export { config };
