# ibm-cos-bucket-listing

Create nice directory listings for IBM Cloud Object Storage buckets.

## Prerequisite

The COS bucket must allow CORS for `GET` request.

Example `cors.json` config file.

```json
{
  "CORSRules": [
    {
      "AllowedOrigins": ["*"],
      "AllowedHeaders": ["*"],
      "AllowedMethods": ["GET"],
      "MaxAgeSeconds": 3000
    }
  ]
}
```

```bash
# Run the command below to set CORS configuration
$ ibmcloud cos put-bucket-cors --bucket <bucket_name> --cors-configuration file://<path_to_cors_config_json> --region <region_name>
```

## Config

#### `COS_BUCKET_URL` variable (required)

`https://<bucket_name>.s3.us.cloud-object-storage.appdomain.cloud`

- Use `https`
- Do **NOT** put a trailing `/` at the end

#### `COS_ROOT_DIR` variable (optional)

Default to `CI_PROJECT_NAME` from GitLab CI [predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

Valid options = `''` (default) or `'SUBDIR_L1/'` or `'SUBDIR_L1/SUBDIR_L2/'` or etc.

- Do **NOT** put a leading '/', e.g. `'/SUBDIR_L1/'`
- Do **NOT** omit the trailing '/', e.g. `'SUBDIR_L1'`

This will disallow navigation shallower than your set directory.

### `ASSET_PREFIX` variable (required)

If hosting under Gitlab Pages or Github Pages, a prefix is required for static asset. Check out [next.js documentation](https://nextjs.org/docs/api-reference/next.config.js/cdn-support-with-asset-prefix)

Valid options = `''`, `/project-name`, or `https://cdn.example.com`

### `SITE_TITLE` variable (optional)

Default `'Object Storage Directory'` or `CI_PROJECT_TITLE` from GitLab CI [predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

## Run

```bash
# Install dependencies
$ yarn install

# Config environment variable

# Build and export static website
$ yarn build
```

## Deploy

The site needs to be rebuilt whenever there is a change make to the COS bucket since the content is fetched at build time.

## Authors

- IBM Developer Skills Network
